package pl.sda.service;

import pl.sda.model.Employee;

import java.util.List;

public interface EmployeeService {

  void save(Employee employee);

  Employee getById(int id);

  List<Employee> getAll();

  void delete(int id);

  void update(Employee emp);

}
