package pl.sda.service.impl;

import pl.sda.dao.UserDao;
import pl.sda.dao.impl.UserDaoImpl;
import pl.sda.model.User;
import pl.sda.service.UserService;

import java.util.List;

public class UserServiceImpl implements UserService {

    private UserDao userDao;

    public UserServiceImpl() {
        this.userDao = new UserDaoImpl();
    }

    @Override
    public void save(User user) {
        userDao.openCurrentSessionwithTransaction();
        userDao.persist(user);
        userDao.closeCurrentSessionwithTransaction();
    }

    @Override
    public User getById(int id) {
        userDao.openCurrentSessionwithTransaction();
        User user = userDao.getById(id);
        userDao.closeCurrentSessionwithTransaction();
        return user;
    }

    @Override
    public List<User> getAll() {
        userDao.openCurrentSessionwithTransaction();
        List<User> users = userDao.getAll();
        userDao.closeCurrentSessionwithTransaction();
        return users;
    }

    @Override
    public void delete(int id) {
        userDao.openCurrentSessionwithTransaction();
        User user = userDao.getById(id);
        if (user != null)
            userDao.delete(user);
        userDao.closeCurrentSessionwithTransaction();
    }

    @Override
    public void update(User user) {
        userDao.openCurrentSessionwithTransaction();
        userDao.update(user);
        userDao.closeCurrentSessionwithTransaction();
    }
}
