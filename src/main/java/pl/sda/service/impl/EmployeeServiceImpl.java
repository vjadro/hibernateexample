package pl.sda.service.impl;

import pl.sda.dao.EmployeeDao;
import pl.sda.model.Employee;
import pl.sda.dao.impl.EmployeeDaoImpl;
import pl.sda.service.EmployeeService;

import java.util.List;

public class EmployeeServiceImpl implements EmployeeService {

  private EmployeeDao employeeDao;

  public EmployeeServiceImpl(){
    this.employeeDao = new EmployeeDaoImpl();
  }

  public void save(Employee employee){
    employeeDao.openCurrentSessionwithTransaction();
    employeeDao.persist(employee);
    employeeDao.closeCurrentSessionwithTransaction();
  }

  public Employee getById(int id){
    employeeDao.openCurrentSessionwithTransaction();
    Employee employee = employeeDao.getById(id);
    employeeDao.closeCurrentSessionwithTransaction();

    return employee;
  }

  public List<Employee> getAll(){
    employeeDao.openCurrentSessionwithTransaction();
    List<Employee> employees = employeeDao.getAll();
    employeeDao.closeCurrentSessionwithTransaction();

    return employees;
  }

  public void delete(int id){
    employeeDao.openCurrentSessionwithTransaction();
    Employee employee = employeeDao.getById(id);
    employeeDao.delete(employee);
    employeeDao.closeCurrentSessionwithTransaction();
  }

  public void update(Employee emp){
    employeeDao.openCurrentSessionwithTransaction();
    employeeDao.update(emp);
    employeeDao.closeCurrentSessionwithTransaction();
  }

}

