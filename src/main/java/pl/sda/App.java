package pl.sda;

import pl.sda.model.Employee;
import pl.sda.model.User;
import pl.sda.service.EmployeeService;
import pl.sda.service.UserService;
import pl.sda.service.impl.EmployeeServiceImpl;
import pl.sda.service.impl.UserServiceImpl;

import java.time.LocalDate;

public class App {
    public static void main( String[] args ){
        UserService userService = new UserServiceImpl();

        userService.delete(1);

        userService.getAll().forEach(user -> System.out.println(user.getFirstName()));
    }
}
