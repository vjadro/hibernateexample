package pl.sda.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class Employee {

  @Id
  @GeneratedValue(strategy= GenerationType.IDENTITY)
  @Column
  private int id;

  @Column(name="NAME", length=20)
  private String name;

  @Column(name="ROLE", length=20)
  private String role;

  @Column(name="insert_time")
  private LocalDate insertTime;

  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public String getRole() {
    return role;
  }
  public void setRole(String role) {
    this.role = role;
  }

  public void setInsertTime(LocalDate insertTime) {
    this.insertTime = insertTime;
  }

  public LocalDate getInsertTime() {
    return insertTime;
  }

  @Override
  public String toString() {
    return "Employee{" +
      "id=" + id +
      ", name='" + name + '\'' +
      ", role='" + role + '\'' +
      ", insertTime=" + insertTime +
      '}';
  }
}