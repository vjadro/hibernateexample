package pl.sda.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import pl.sda.dao.EmployeeDao;
import pl.sda.model.Employee;

import java.util.List;

public class EmployeeDaoImpl implements EmployeeDao {

  private Session currentSession;
  private Transaction currentTransaction;

  public Session openCurrentSession() {
    currentSession = getSessionFactory().openSession();
    return currentSession;
  }

  public Session openCurrentSessionwithTransaction() {
    currentSession = getSessionFactory().openSession();
    currentTransaction = currentSession.beginTransaction();
    return currentSession;
  }

  public SessionFactory getSessionFactory() {
    Configuration configuration = new Configuration();
    configuration.configure("/hibernate.cfg.xml");
    SessionFactory sessionFactory = configuration.buildSessionFactory();

    return sessionFactory;
  }

  public void closeCurrentSession() {
    currentSession.close();
  }

  public void closeCurrentSessionwithTransaction() {
    currentTransaction.commit();
    currentSession.close();
  }

  public Session getCurrentSession() {
    return currentSession;
  }

  public Transaction getCurrentTransaction() {
    return currentTransaction;
  }

  public void persist(Employee emp){
    getCurrentSession().persist(emp);
  }

  public Employee getById(int id) {
    return getCurrentSession().get(Employee.class, id);
  }

  public List<Employee> getAll() {
    return getCurrentSession().createQuery("from Employee").list();
  }

  public void delete(Employee emp){
    getCurrentSession().delete(emp);
  }

  public void update(Employee emp){
    getCurrentSession().update(emp);
  }
}
