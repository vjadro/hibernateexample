package pl.sda.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import pl.sda.dao.BaseDao;

import java.util.List;


public abstract class BaseDaoImpl<T> implements BaseDao<T> {

    private Class<T> clazz;

    private Session currentSession;
    private Transaction currentTransaction;

    protected BaseDaoImpl(Class<T> clazz) {
        this.clazz = clazz;
    }

    public Session openCurrentSession() {
        currentSession = getSessionFactory().openSession();
        return currentSession;
    }

    public Session openCurrentSessionwithTransaction() {
        currentSession = getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public SessionFactory getSessionFactory() {
        Configuration configuration = new Configuration();
        configuration.configure("/hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        return sessionFactory;
    }

    public void closeCurrentSession() {
        currentSession.close();
    }

    public void closeCurrentSessionwithTransaction() {
        currentTransaction.commit();
        currentSession.close();
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    public Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    @Override
    public void persist(T obj) {
        getCurrentSession().persist(obj);
    }

    @Override
    public T getById(int id) {
        return getCurrentSession().get(clazz,id);
    }

    @Override
    public List<T> getAll() {
        return getCurrentSession().createQuery("from " + clazz.getName()).list();
    }

    @Override
    public void delete(T obj) {
        getCurrentSession().delete(obj);
    }

    @Override
    public void update(T obj) {
        getCurrentSession().update(obj);
    }

}
