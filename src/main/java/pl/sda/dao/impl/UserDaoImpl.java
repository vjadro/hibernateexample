package pl.sda.dao.impl;

import pl.sda.dao.UserDao;
import pl.sda.model.User;

public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao {

    public UserDaoImpl() {
        super(User.class);
    }
}
