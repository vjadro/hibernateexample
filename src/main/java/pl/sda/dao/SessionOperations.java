package pl.sda.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public interface SessionOperations {

  Session openCurrentSession();
  Session openCurrentSessionwithTransaction();

  void closeCurrentSessionwithTransaction();
  void closeCurrentSession();

  SessionFactory getSessionFactory();
  Transaction getCurrentTransaction();

}
