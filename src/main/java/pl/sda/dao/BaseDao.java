package pl.sda.dao;

import java.util.List;

public interface BaseDao<T> extends SessionOperations {

    void persist(T obj);

    T getById(int id);

    List<T> getAll();

    void delete(T obj);

    void update(T obj);
}
