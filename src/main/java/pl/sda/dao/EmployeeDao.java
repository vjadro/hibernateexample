package pl.sda.dao;

import pl.sda.model.Employee;

import java.util.List;

public interface EmployeeDao extends SessionOperations {

  void persist(Employee employee);

  Employee getById(int id);

  List<Employee> getAll();

  void delete(Employee emp);

  void update(Employee emp);

}
